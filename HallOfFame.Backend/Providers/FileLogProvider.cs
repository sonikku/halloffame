using HallOfFame.Backend.Services.Implementations;
using Microsoft.Extensions.Logging;

namespace HallOfFame.Backend.Providers
{
    public class FileLogProvider : ILoggerProvider
    {
        private string _path;

        public FileLogProvider(string path)
        {
            _path = path;
        }

        public ILogger CreateLogger(string category)
        {
            return new FileLogService(_path);
        }
        
        public void Dispose() {}
    }
}