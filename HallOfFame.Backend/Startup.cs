using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using HallOfFame.Backend.Contexts;
using HallOfFame.Backend.Extensions;
using HallOfFame.Backend.Repositories.Implementations;
using HallOfFame.Backend.Repositories.Interfaces;
using HallOfFame.Backend.Services.Implementations;
using HallOfFame.Backend.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.VisualBasic;

namespace HallOfFame.Backend
{
    public class Startup
    {
        private IConfiguration Configuration { get; }

        public Startup(IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, true);

            if (env.IsDevelopment())
            {
                builder.AddUserSecrets<Startup>();
            }

            Configuration = builder.Build();
        }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            var dbConfiguration = new SqlConnectionStringBuilder(Configuration
                .GetConnectionString("ConnectionString"))
            {
                Password = Configuration["HallOfFameDbPassword"]
            };

            services.AddDbContext<PersonContext>(options =>
                options.UseSqlServer(dbConfiguration.ConnectionString));
            services.AddScoped<IPersonRepository, PersonRepository>();
            services.AddScoped<IPersonService, PersonService>();

            services.AddSwaggerGen();
        }

        public void Configure(IApplicationBuilder app, 
            IWebHostEnvironment env, 
            ILoggerFactory loggerFactory)
        {
            loggerFactory.AddFile(
                Path.Combine(
                    Environment.CurrentDirectory, 
                    DateTime.Now.ToString("o",CultureInfo.InvariantCulture) +
                    ".log"));
            var logger = loggerFactory.CreateLogger(nameof(FileLogService));

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.Use((context, next) =>
            {
                context.Request.EnableBuffering();
                return next();
            });
            
            app.UseRouting();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Hall Of Fame API");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}