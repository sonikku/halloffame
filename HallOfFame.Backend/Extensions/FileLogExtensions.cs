using System;
using System.Text;
using HallOfFame.Backend.Providers;
using Microsoft.Extensions.Logging;

namespace HallOfFame.Backend.Extensions
{
    public static class FileLogExtensions
    {
        public static ILoggerFactory AddFile(this ILoggerFactory factory,
            string path)
        {
            factory.AddProvider(new FileLogProvider(path));
            return factory;
        }

        private static string CreateErrorMessage(string message, string dateFormat)
        {
            var builder = new StringBuilder();
            builder.Append('[');
            builder.Append(DateTime.Now.ToString(dateFormat));
            builder.Append(']');
            builder.Append(':');
            builder.Append('"');
            builder.Append(message);
            builder.Append('"');
            message = builder.ToString();
            return message;
        }
        
        public static void LogError(this ILogger logger, 
            Exception ex,
            string message, 
            string dateFormat, 
            params object[] args)
        {
            logger.LogError(ex, CreateErrorMessage(message, dateFormat), args);
        }
        
        public static void LogError(this ILogger logger, 
            string message, 
            string dateFormat, 
            params object[] args)
        {
            logger.LogError(CreateErrorMessage(message, dateFormat), args);
        }
    }
}