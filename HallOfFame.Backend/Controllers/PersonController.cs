using System.Collections.Generic;
using HallOfFame.Backend.Models.Json;
using HallOfFame.Backend.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using static System.String;

namespace HallOfFame.Backend.Controllers
{
    [ApiController]
    public class PersonController : ControllerBase
    {
        private static string ErrorNullValues =>
            "Person should have null id in the body and not-null skills array";

        private static string ErrorIncorrectId(long id) =>
            $"Couldn't find person with id {id}";
        
        private IPersonService PersonService { get; }

        private ILogger<PersonController> Logger { get; }

        public PersonController(IPersonService personService, ILogger<PersonController> logger)
        {
            PersonService = personService;
            Logger = logger;
        }

        /// <summary>
        /// Возвращает список всех сотрудников.
        /// </summary>
        /// <returns>Список сотрудников</returns>
        [HttpGet]
        [Route("/api/v1/persons")]
        public IActionResult GetPersons()
        {
            var persons = PersonService.GetPersons();
            return persons == null ? new ObjectResult(new List<Person>()) : new ObjectResult(persons);
        }

        /// <summary>
        /// Возвращает сотрудника с указанным ID.
        /// </summary>
        /// <param name="id">ID сотрудника.</param>
        /// <returns>Сотрудник.</returns>
        [HttpGet]
        [Route("/api/v1/persons/{id}")]
        public IActionResult GetPerson(long id)
        {
            var person = PersonService.GetPersonById(id);
            return person == null ? 
                NotFound(ErrorIncorrectId(id)) : 
                new ObjectResult(person);
        }
        
        /// <summary>
        /// Добавляет сотрудника в список.
        /// </summary>
        /// <param name="person">Объект сотрудника.
        /// ID должен быть null или не должен быть указан.</param>
        /// <returns>Объект добавленного сотрудника.</returns>
        [HttpPost]
        [Route("/api/v1/persons")]
        public ActionResult<Person> AddPerson(Person person)
        {
            if (person.Id != null || person.Skills == null)
            {
                return BadRequest(ErrorNullValues);
            }
            
            var returned = PersonService.AddPerson(person);

            if (returned == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            return CreatedAtAction(nameof(AddPerson), 
                new {id = person.Id}, 
                person);
        }

        /// <summary>
        /// Обновляет данные указанного сотрудника.
        /// </summary>
        /// <param name="id">ID сотрудника.</param>
        /// <param name="person">Объект сотрудника.
        /// ID должен быть null или не должен быть указан.</param>
        /// <returns>Объект добавленного сотрудника.</returns>
        [HttpPut]
        [Route("/api/v1/persons/{id:}")]
        public ActionResult PutPerson(long id, Person person)
        {
            if (person.Id != null || person.Skills == null)
            {
                return BadRequest(ErrorNullValues);
            }

            person.Id = id;
            if (PersonService.UpdatePerson(person) != null)
            {
                return Ok(person);
            }
            else
            {
                return NotFound(ErrorIncorrectId(id));
            }
        }

        /// <summary>
        /// Удаляет сотрудника из списка.
        /// </summary>
        /// <param name="id">ID сотрудника.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("/api/v1/persons/{id}")]
        public ActionResult DeletePerson(long id)
        {
            bool deleted = PersonService.DeletePerson(id);
            if (!deleted)
            {
                return NotFound(ErrorIncorrectId(id));
            }

            return NoContent();
        }
    }
}