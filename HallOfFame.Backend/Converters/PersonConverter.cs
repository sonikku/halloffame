using System.Collections.Generic;
using HallOfFame.Backend.Models.Json;

namespace HallOfFame.Backend.Converters
{
    /// <summary>
    /// Класс, содержащий конвертеры для приведения
    /// JSON-модели сотрудников и навыков к моделям
    /// для БД.
    /// </summary>
    public static class PersonConverter
    {
        #region From DB to JSON
        
        public static Person ConvertPersonDbToJson(Models.Database.Person personDb)
        {
            var person = new Person()
            {
                Name = personDb.Name,
                DisplayName = personDb.DisplayName,
                Id = personDb.Id,
                Skills = ConvertSkillsFromDbToJson(personDb.Skills)
            };
            return person;
        }
        
        public static List<Person> ConvertPersonsDbToJson(List<Models.Database.Person> personsDb)
        {
            var persons = new List<Person>();
            personsDb.ForEach(i =>
            {
                persons.Add(ConvertPersonDbToJson(i));
            });
            return persons;
        }

        public static Skill ConvertSkillFromDbToJson(Models.Database.SkillPossession possessionDb)
        {
            var skill = new Skill()
            {
                Level = possessionDb.Level,
                Name = possessionDb.Skill.Name
            };
            return skill;
        }
        
        public static List<Skill> ConvertSkillsFromDbToJson(
            IEnumerable<Models.Database.SkillPossession> possessionsDb)
        {
            var skills = new List<Skill>();
            foreach (var p in possessionsDb)
            {
                skills.Add(ConvertSkillFromDbToJson(p));
            }
            return skills;
        }
        
        #endregion
        
        #region From JSON to DB
        
        
        #endregion
    }
}