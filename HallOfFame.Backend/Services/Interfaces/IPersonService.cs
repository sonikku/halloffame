using System.Collections.Generic;
using HallOfFame.Backend.Models;
using HallOfFame.Backend.Models.Json;

namespace HallOfFame.Backend.Services.Interfaces
{
    /// <summary>
    /// Интерфейс сервиса работы с сущностями сотрудников
    /// </summary>
    public interface IPersonService
    {
        public List<Person> GetPersons();
        public Person GetPersonById(long id);
        public Person AddPerson(Person person);
        public Person UpdatePerson(Person person);
        public bool DeletePerson(long id);
    }
}