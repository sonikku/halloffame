using System;
using System.Collections.Generic;
using System.Linq;
using HallOfFame.Backend.Converters;
using HallOfFame.Backend.Models.Database;
using HallOfFame.Backend.Repositories.Interfaces;
using HallOfFame.Backend.Services.Interfaces;
using Microsoft.Extensions.Logging;
using ModelsJson = HallOfFame.Backend.Models.Json;
using ModelsDb = HallOfFame.Backend.Models.Database;
using HallOfFame.Backend.Extensions;

namespace HallOfFame.Backend.Services.Implementations
{
    /// <summary>
    /// Реализация сервиса работы с сущностями сотрудников
    /// </summary>
    public class PersonService : IPersonService
    {
        /// <summary>
        /// Репозиторий сотрудников
        /// </summary>
        private IPersonRepository Persons { get; }
        
        private ILogger<PersonService> Logger { get; }

        public PersonService(IPersonRepository personRepository, ILogger<PersonService> logger)
        {
            Persons = personRepository;
            Logger = logger;
        }

        private List<SkillPossession> CreateSkillPossessions(ModelsJson.Person personJson, 
            ModelsDb.Person personDb)
        {
            if (personJson.Skills.Count == 0) return new List<SkillPossession>();
            var skillsDb = Persons.GetSkills() ?? new List<Skill>();
            var possessionsDb = new List<SkillPossession>();

            try
            {
                foreach (var skill in personJson.Skills)
                {
                    var foundSkill = skillsDb
                        .FirstOrDefault(i => i.Name == skill.Name);
                    var possessionDb = new SkillPossession()
                    {
                        Level = skill.Level,
                        Person = personDb,
                        Skill =
                            foundSkill ??
                            new Skill()
                            {
                                Name = skill.Name
                            },
                        SkillId = foundSkill?.Id ?? 0
                    };
                    possessionsDb.Add(possessionDb);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Error: " + ex.Message, "u");
            }
            
            return possessionsDb;
        }
        
        /// <summary>
        /// Получает список всех сотрудников
        /// </summary>
        /// <returns>Объект Request со списком сотрудников внутри.
        /// Если получить список сотрудников не удалось, свойство
        /// Handled будет равно false.</returns>
        public List<ModelsJson.Person> GetPersons()
        {
            var personsDb = Persons.GetAllPersonsFromDb();
            return personsDb == null ? 
                new List<ModelsJson.Person>() : 
                PersonConverter.ConvertPersonsDbToJson(personsDb);
        }

        /// <summary>
        /// Получает сотрудника по его ID
        /// </summary>
        /// <returns>Объект Request с сотрудником внутри.
        /// Если получить сотрудника не удалось, свойство
        /// Handled будет равно false.</returns>
        public ModelsJson.Person GetPersonById(long id)
        {
            if (id < 1) return null;
            var personDb = Persons.GetPersonById(id);
            return personDb == null ? 
                null : 
                PersonConverter.ConvertPersonDbToJson(personDb);
        }

        /// <summary>
        /// Добавляет сотрудника в базу данных
        /// </summary>
        /// <param name="person">Объект сотрудника (JSON-модель)</param>
        /// <returns>Истина, если добавить удалось, иначе ложь</returns>
        public ModelsJson.Person AddPerson(ModelsJson.Person person)
        {
            var personDb = new ModelsDb.Person()
            {
                Name = person.Name,
                DisplayName = person.DisplayName
            };
            ModelsDb.Person returned;
            if (person.Skills.Count > 0)
            {
                var possessionsDb = CreateSkillPossessions(person, personDb);
                returned = Persons.AddPerson(possessionsDb, personDb);
            }
            else
            {
                returned = Persons.AddPerson(personDb);
            }

            person.Id = personDb.Id;
            return returned != null ? person : null;
        }

        /// <summary>
        /// Обновляет данные сотрудника в базе данных.
        /// </summary>
        /// <param name="person">Объект сотрудника (JSON-модель).</param>
        /// <returns>Истина, если обновить удалось, иначе ложь.</returns>
        public ModelsJson.Person UpdatePerson(ModelsJson.Person person)
        {
            if (person.Id == null)
            {
                return null;
            }
            var personDb = Persons.GetPersonById(person.Id.Value);
            if (personDb == null)
            {
                Logger.LogError($"Couldn't get person " +
                                $"with id {person.Id} from DB to update.", 
                    "u");
                return null;
            }

            personDb.Name = person.Name;
            personDb.DisplayName = person.DisplayName;
            personDb.Skills = CreateSkillPossessions(person, personDb);
            
            var oldSkills = Persons.GetSkillPossessions(personDb);
            Persons.DeleteSkillPossessions(oldSkills);
            Persons.UpdatePerson(personDb);
            
            person = PersonConverter.ConvertPersonDbToJson(personDb);
            
            return person;
        }

        /// <summary>
        /// Удаляет сотрудника из базы данных
        /// </summary>
        /// <param name="id">ID сотрудника</param>
        /// <returns>Истина, если удалить удалось, иначе ложь</returns>
        public bool DeletePerson(long id) => Persons.DeletePerson(id);
    }
}