using System;
using System.IO;
using Microsoft.Extensions.Logging;

namespace HallOfFame.Backend.Services.Implementations
{
    public class FileLogService : ILogger
    {
        private string _filePath;
        private static object _lockObj = new object();

        public FileLogService(string path)
        {
            _filePath = path;
        }
        
        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return logLevel == LogLevel.Error || 
                   logLevel == LogLevel.Critical;
        }

        public void Log<TState>(LogLevel logLevel, 
            EventId eventId, 
            TState state, 
            Exception exception, 
            Func<TState, Exception, string> formatter)
        {
            if (formatter != null)
            {
                lock (_lockObj)
                {
                    File.AppendAllText(_filePath, formatter(state, exception) + 
                                                  Environment.NewLine);
                }
            }
        }
    }
}