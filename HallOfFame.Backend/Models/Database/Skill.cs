using System.Collections.Generic;

namespace HallOfFame.Backend.Models.Database
{
    /// <summary>
    /// Класс навыка для БД.
    /// </summary>
    public class Skill
    {
        /// <summary>
        /// Название навыка.
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// ID навыка в таблице.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Список отношений для реализации связи MtM.
        /// </summary>
        public IEnumerable<SkillPossession> SkillPossessions { get; set; } = 
            new List<SkillPossession>();
    }
}