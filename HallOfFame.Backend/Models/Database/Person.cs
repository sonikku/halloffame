using System.Collections.Generic;

namespace HallOfFame.Backend.Models.Database
{
    /// <summary>
    /// Класс сотрудника для БД.
    /// </summary>
    public class Person
    {
        /// <summary>
        /// Имя сотрудника.
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Отображаемое имя сотрудника.
        /// </summary>
        public string DisplayName { get; set; }
        
        /// <summary>
        /// ID сотрудника.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Список отношений для реализации связи MtM.
        /// </summary>
        public IEnumerable<SkillPossession> Skills { get; set; } 
            = new List<SkillPossession>();
    }
}