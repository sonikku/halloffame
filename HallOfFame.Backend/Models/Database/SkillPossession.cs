using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HallOfFame.Backend.Models.Database
{
    /// <summary>
    /// Класс отношения сотрудника к навыку. Используется
    /// как связывание для реализации связи MtM в БД.
    /// </summary>
    public class SkillPossession
    {
        /// <summary>
        /// ID записи связывания.
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// Объект сотрудника.
        /// </summary>
        public Person Person { get; set; }
        
        /// <summary>
        /// ID сотрудника
        /// </summary>
        public long PersonId { get; set; }
        
        /// <summary>
        /// Объект навыка.
        /// </summary>
        public Skill Skill { get; set; }
        
        /// <summary>
        /// ID навыка.
        /// </summary>
        public long SkillId { get; set; }

        /// <summary>
        /// Уровень навыка.
        /// </summary>
        public byte Level { get; set; }
    }
}