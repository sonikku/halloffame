using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace HallOfFame.Backend.Models.Json
{
    /// <summary>
    /// Класс навыка человека
    /// </summary>
    public class Skill
    {
        /// <summary>
        /// Минимальный уровень навыка
        /// </summary>
        private static readonly byte MinLevel = 1;
        
        /// <summary>
        /// Максимальный уровень навыка
        /// </summary>
        private static readonly byte MaxLevel = 10;
        
        private byte _level;
        
        /// <summary>
        /// Название навыка
        /// </summary>
        [JsonProperty("name")]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Уровень навыка
        /// </summary>
        [JsonProperty("level")]
        [Required]
        public byte Level
        {
            get => _level;
            set
            {
                if ((value < MinLevel) || (value > MaxLevel))
                {
                    throw new ArgumentException("Invalid skill level");
                }

                _level = value;
            }
        }
    }
}