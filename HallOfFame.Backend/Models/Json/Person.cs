using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace HallOfFame.Backend.Models.Json
{
    /// <summary>
    /// Класс личности
    /// </summary>
    public class Person
    {
        /// <summary>
        /// ID
        /// </summary>
        [JsonProperty("id")]
        public long? Id { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
        
        /// <summary>
        /// Отображаемое имя
        /// </summary>
        [JsonProperty("displayName")]
        public string DisplayName { get; set; }
        
        /// <summary>
        /// Список навыков
        /// </summary>
        [JsonProperty("skills")]
        public virtual ICollection<Skill> Skills { get; set; }

        public Person()
        {
            Skills = new List<Skill>();
        }
    }
}