using System;
using System.Collections.Generic;
using System.Linq;
using HallOfFame.Backend.Contexts;
using HallOfFame.Backend.Extensions;
using HallOfFame.Backend.Models.Database;
using HallOfFame.Backend.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace HallOfFame.Backend.Repositories.Implementations
{
    /// <summary>
    /// Реализация репозитория сотрудников
    /// </summary>
    public class PersonRepository : IPersonRepository
    {
        /// <summary>
        /// Контекст в БД
        /// </summary>
        private PersonContext Context { get; }
        
        private ILogger<PersonRepository> Logger { get; }
        
        public PersonRepository(PersonContext context, ILogger<PersonRepository> logger)
        {
            Context = context;
            Logger = logger;
        }

        /// <summary>
        /// Получает из связывающей таблицы список отношений сотрудников
        /// к навыкам, включая их уровни.
        /// </summary>
        /// <param name="person">Объект сотрудника.</param>
        /// <returns>Список отношений сотрудников к навыкам.</returns>
        public List<SkillPossession> GetSkillPossessions(Person person)
        {
            // Если в переданном объекте список навыков равен null,
            // то возвращается пустой список.
            if (person.Skills == null)
            {
                return new List<SkillPossession>();
            }

            return Context
                .SkillPossession
                .Where(i => i.PersonId == person.Id)
                .ToList();
        }

        /// <summary>
        /// Получает всех сотрудников из базы данных и сохраняет их
        /// в объекте Request.
        /// </summary>
        /// <returns>Объект Requst, содержащий список сотрудников.
        /// Если получить не удалось, Handled будет равен false.</returns>
        public List<Person> GetAllPersonsFromDb()
        {
            List<Person> items = null;
            try
            {
                items = Context
                    .Person
                    .Include(i => i.Skills)
                    .ThenInclude(k => k.Skill)
                    .ToList();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex,
                    "Couldn't get persons from database: " 
                                + ex.Message,
                    "u");
                return null;
            }
            return items;
        }
            
        /// <summary>
        /// Получает сотрудника по его ID и сохраняет его
        /// в объекте Request.
        /// </summary>
        /// <param name="id">ID сотрудника</param>
        /// <returns>Объект Requst, содержащий объект сотрудника.
        /// Если получить не удалось, Handled будет равен false.</returns>
        public Person GetPersonById(long id)
        {
            Person person;
            try
            {
                person = Context
                    .Person
                    .Include(i => i.Skills)
                    .ThenInclude(k => k.Skill)
                    .FirstOrDefault(p => p.Id == id);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, 
                    $"Couldn't get person with ID {id} from database: " 
                                + ex.Message,
                    "u");
                return null;
            }
            return person;
        }
            
        /// <summary>
        /// Добавляет сотрудника без навыков в БД.
        /// </summary>
        /// <param name="person">Объект сотрудника.</param>
        /// <returns>Истина, если удалось добавить, иначе ложь.</returns>
        public Person AddPerson(Person person)
        {
            try
            { 
                Context.Person.Add(person);
                Context.SaveChanges();
                return person;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex,
                    $"Couldn't add person to database: " 
                                + ex.Message,
                    "u");
                return null;
            }
        }

        /// <summary>
        /// Добавляет сотрудника с навыками в БД.
        /// </summary>
        /// <param name="possessions">Список отношений сотрудника к навыкам.</param>
        /// <returns>Истина, если удалось добавить, иначе ложь.</returns>
        public Person AddPerson(List<SkillPossession> possessions, Person person)
        {
            try
            {
                Context.SkillPossession.AddRange(possessions);
                Context.SaveChanges();
                return person;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex,
                    $"Couldn't add person to database: " 
                                + ex.Message,
                "u");
                return null;
            }
        }
        
        /// <summary>
        /// Обновляет данные сотрудника в БД.
        /// </summary>
        /// <param name="person">Объект сотрудника.</param>
        /// <returns>Истина, если удалось обновить, иначе ложь.</returns>
        public Person UpdatePerson(Person person)
        {
            try
            {
                Context.Person.Update(person);
                Context.SaveChanges();
                return person;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex,
                    $"Couldn't update person in database: " 
                                + ex.Message,
                    "u");
                return null;
            }
        }

        /// <summary>
        /// Удаляет сотрудника из БД.
        /// </summary>
        /// <param name="person">Объект сотрудника (JSON-модель).</param>
        /// <returns>Истина, если удалось удалить, иначе ложь.</returns>
        public bool DeletePerson(Person person)
        {
            return DeletePerson(person.Id);
        }

        /// <summary>
        /// Удаляет сотрудника из БД.
        /// </summary>
        /// <param name="id">ID сотрудника.</param>
        /// <returns>Истина, если удалось удалить, иначе ложь.</returns>
        public bool DeletePerson(long id)
        {
            try
            {
                // Поиск сотрудника в БД.
                var personDb = Context.Person.FirstOrDefault(i => i.Id == id);
                // Если не найден, вернуть false.
                if (personDb == null) return false;

                // Поиск отношений данного сотрудника ко своим навыкам
                var possessionsDb = GetSkillPossessions(personDb);
                // Удаление отношений.
                Context.SkillPossession.RemoveRange(possessionsDb);
                // Удаление сотрудника.
                Context.Person.Remove(personDb);
                Context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex,
                    $"Couldn't delete person from database: " 
                                + ex.Message,
                    "u");
                return false;
            }
        }

        /// <summary>
        /// Получает список всех отношений сотрудников к навыкам
        /// </summary>
        /// <returns>Список отношений сотрудников к навыкам</returns>
        public List<SkillPossession> GetSkillPossessions()
        {
            try
            {
                return Context.SkillPossession.ToList();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex,
                    $"Couldn't get skill possessions from database: " 
                                + ex.Message,
                    "u");
                return null;
            }
        }

        /// <summary>
        /// Получает список всех навыков
        /// </summary>
        /// <returns>Список навыков</returns>
        public List<Skill> GetSkills()
        {
            try
            {
                return Context.Skill.ToList();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex,
                    $"Couldn't get skills from database: " 
                                + ex.Message,
                    "u");
                return null;
            }
        }

        /// <summary>
        /// Удаляет заданные отношения сотрудников к навыкам из БД.
        /// </summary>
        /// <param name="possessions">Список отношений сотрудников к навыкам.</param>
        /// <returns>Истина, если удалось удалить, иначе ложь.</returns>
        public bool DeleteSkillPossessions(List<SkillPossession> possessions)
        {
            try
            {
                Context.SkillPossession.RemoveRange(possessions);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex,
                    $"Couldn't delete skill possessions from database: " 
                                + ex.Message,
                    "u");
                return false;
            }
            
            return true;
        }
    }
}