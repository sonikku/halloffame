using System.Collections.Generic;
using HallOfFame.Backend.Models;
using HallOfFame.Backend.Models.Database;

namespace HallOfFame.Backend.Repositories.Interfaces
{
    /// <summary>
    /// Интерфейс репозитория сотрудников
    /// </summary>
    public interface IPersonRepository
    {
        /// <summary>
        /// Получает всех сотрудников из базы данных и сохраняет их
        /// в объекте Request.
        /// </summary>
        /// <returns>Объект Requst, содержащий список сотрудников.
        /// Если получить не удалось, Handled будет равен false.</returns>
        public List<Person> GetAllPersonsFromDb();
        
        /// <summary>
        /// Получает сотрудника по его ID и сохраняет его
        /// в объекте Request.
        /// </summary>
        /// <param name="id">ID сотрудника</param>
        /// <returns>Объект Requst, содержащий объект сотрудника.
        /// Если получить не удалось, Handled будет равен false.</returns>
        public Person GetPersonById(long id);
        
        /// <summary>
        /// Добавляет сотрудника без навыков в БД.
        /// </summary>
        /// <param name="person">Объект сотрудника (JSON-модель).</param>
        /// <returns>Истина, если удалось добавить, иначе ложь.</returns>
        public Person AddPerson(Person person);

        /// <summary>
        /// Добавляет сотрудника с навыками в БД.
        /// </summary>
        /// <param name="person">Объект сотрудника (JSON-модель).</param>
        /// <returns>Истина, если удалось добавить, иначе ложь.</returns>
        public Person AddPerson(List<SkillPossession> possessionsDb, Person person);
        
        /// <summary>
        /// Обновляет данные сотрудника в БД.
        /// </summary>
        /// <param name="person">Объект сотрудника (JSON-модель).</param>
        /// <returns>Истина, если удалось обновить, иначе ложь.</returns>
        public Person UpdatePerson(Person person);
        
        /// <summary>
        /// Удаляет сотрудника из БД.
        /// </summary>
        /// <param name="person">Объект сотрудника (JSON-модель).</param>
        /// <returns>Истина, если удалось удалить, иначе ложь.</returns>
        public bool DeletePerson(Person person);
        
        /// <summary>
        /// Удаляет сотрудника из БД.
        /// </summary>
        /// <param name="id">ID сотрудника.</param>
        /// <returns>Истина, если удалось удалить, иначе ложь.</returns>
        public bool DeletePerson(long id);

        /// <summary>
        /// Получает список всех отношений сотрудников к навыкам.
        /// </summary>
        /// <returns>Список отношений сотрудников к навыкам.</returns>
        public List<SkillPossession> GetSkillPossessions();
        
        /// <summary>
        /// Получает список отношений сотрудника к его навыкам.
        /// </summary>
        /// <param name="person">Объект сотрудника.</param>
        /// <returns>Список отношений сотрудника к его навыкам.</returns>
        public List<SkillPossession> GetSkillPossessions(Person person);

        /// <summary>
        /// Получает список всех навыков.
        /// </summary>
        /// <returns>Список навыков.</returns>
        public List<Skill> GetSkills();

        /// <summary>
        /// Удаляет заданные отношения сотрудников к навыкам из БД.
        /// </summary>
        /// <param name="possessions">Список отношений сотрудников к навыкам.</param>
        /// <returns>Истина, если удалось удалить, иначе ложь.</returns>
        public bool DeleteSkillPossessions(List<SkillPossession> possessions);
    }
}