﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HallOfFame.Backend.Migrations
{
    public partial class CascadeDelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SkillPossession_Skill_SkillId",
                table: "SkillPossession");

            migrationBuilder.AddForeignKey(
                name: "FK_SkillPossession_Skill_SkillId",
                table: "SkillPossession",
                column: "SkillId",
                principalTable: "Skill",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SkillPossession_Skill_SkillId",
                table: "SkillPossession");

            migrationBuilder.AddForeignKey(
                name: "FK_SkillPossession_Skill_SkillId",
                table: "SkillPossession",
                column: "SkillId",
                principalTable: "Skill",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
