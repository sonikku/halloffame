﻿using System;
using HallOfFame.Backend.Models.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace HallOfFame.Backend.Contexts
{
    public partial class PersonContext : DbContext
    {
        public DbSet<Person> Person { get; set; }
        public DbSet<Skill> Skill { get; set; }
        public DbSet<SkillPossession> SkillPossession { get; set; }
        
        public PersonContext()
        {
            
        }

        public PersonContext(DbContextOptions<PersonContext> options)
            : base(options)
        {
            // Проверка наличия базы данных.
            // При использовании dotnet ef migrations add необходимо закомментировать.
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Name=ConnectionString");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", 
                "Cyrillic_General_CI_AS");

            // modelBuilder.Entity<Person>()
            //     .HasKey(k => k.Id)
            //     .HasName("PRIMARY");
            //
            // modelBuilder.Entity<Skill>()
            //     .HasKey(k => k.Id)
            //     .HasName("PRIMARY");
            
            modelBuilder.Entity<SkillPossession>(entity =>
            {
                entity.HasOne(p => p.Person)
                    .WithMany(sp => sp.Skills)
                    .HasForeignKey(fk => fk.PersonId)
                    .OnDelete(DeleteBehavior.ClientCascade);

                entity.HasOne(p => p.Skill)
                    .WithMany(sp => sp.SkillPossessions)
                    .HasForeignKey(fk => fk.SkillId)
                    .OnDelete(DeleteBehavior.ClientCascade);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
